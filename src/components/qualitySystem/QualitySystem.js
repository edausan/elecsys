import React from 'react';
import Wrapper from '../layout/Wrapper'
import ImgWrapper from './ImgWrapper'

const QualitySystem = () => {

    return (

        <Wrapper title="Quality System">
            <ImgWrapper small="/static/images/quality system/1-min.png" src="http://www.elecsysmfg.com/wp-content/uploads/2018/10/1.png" id="wrapper-1" />
            <ImgWrapper small="/static/images/quality system/2-min.png" src="http://www.elecsysmfg.com/wp-content/uploads/2018/10/2.png" id="wrapper-2" />
            <ImgWrapper small="/static/images/quality system/3-min.png" src="http://www.elecsysmfg.com/wp-content/uploads/2018/10/3.png" id="wrapper-3" />
        </Wrapper>
    );
}

export default QualitySystem